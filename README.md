# Events

## Dazugehörige Design Patterns
Observer, Publish/Subscribe

## Was habe ich davon?
Events ermöglichen den effizienten und übersichtlichen Umgang mit Callbacks, wenn Interessenten-Callbacks **beliebig oft** aufgerufen werden können sollen (im Unterschied zu Promises).

Wann verwende ich Events?
Auf dem PC üblicherweise dann, wenn ich über Tastatur- oder Mauseingaben informiert werden möchte (Beispiel: `jQuery(selector).click(callback)`).
Das ist aber nur ein möglicher Anwendungsbereich von vielen. Weitere Anwendungsbereiche:
 * ein Server liefert von Zeit zu Zeit neue Daten, über die ich informiert werden möchte
 * Hooks (z.B. Datenbank-Hooks wie "after create", "after save" usw.)
 * Middleware einer Webapplikation
 * viele weitere Anwendungsfälle, die vergleichbar sind

## Begriffe
 * **Publisher** = Objekt, das Events feuert
 * **Subscriber/Observer** = mein Callback, das über ein Event informiert werden soll (muss keine Funktion sein, kann auch ein Objekt mit Methoden sein)
 * **Subscribe** = mein Callback registrieren (abonnieren)
 * **Unsubscribe** = mein Callback abmelden (Abonnement beenden)
 * **Publish** = alle angemeldeten Callbacks aufrufen, um ihnen etwas mitzuteilen
 * **Notification** = Mitteilung

## Beispiel jQuery
 * Publisher: `var publisher = $({});`
 * Observer: `function onMyEvent(event, data) { ... }`
 * Subscribe: `publisher.on('myEvent', onMyEvent)`
 * Publish: `publisher.trigger('myEvent', data)`
 * Unsubscribe: `publisher.off('myEvent', onMyEvent)`

## Was muss ich beachten, wenn ich von Promises zu Events komme?
(Probleme, die ich mit Promises nicht habe, mit Events aber schon)

### Destruktion
Bevor sich ein Observer-Objekt auflösen soll, weil es nicht mehr benötigt wird (Stichwort Garbage Collection), sollte es seine Abonnements abmelden (unsubscribe).

**Vorsicht:** Damit nicht andere Observer ungewollt mit-abgemeldet werden, sollte man daran denken, das Callback bei der Abmeldung mit-anzugeben.

### Scope (Wirkungsbereich)
Ich sollte mir gut überlegen, ob ich über einen einzigen, globalen Publisher global Events versenden möchte: Es könnte deutlich berechenbarer (nachvollziehbarer und damit debug-barer) sein, viele Publisher-Objekte anzulegen und diese in beschränkten Bereichen des Programms agieren zu lassen.

### Namensgebung
Es gibt meistens nicht nur ein Event-Callback, sondern auch einen Event-Namen. Für diesen sollte ich einen handlichen und merkbaren Wert wählen, denn mit einem Typo im Event-Namen empfange ich keine Events.

## Weiterführende Recherche
 * Welche Publish/Subscribe-Implementierungen gibt es? (jQuery, Angular, ...)
 * Welche Vor- und Nachteile haben sie im Vergleich zu den jeweils anderen?
 * Gibt es andere Design Patterns, die dem Observer-Pattern (also den Events) Konkurrenz machen und überlegen sein können?

 *Christian Niederreiter, 2016-04*
